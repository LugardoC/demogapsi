//
//  LPreviewCollectionCell.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import UIKit
import Kingfisher

class LPreviewCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgStars: UIImageView!
    @IBOutlet weak var imgRatings: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    
    var setData : LProduct? = nil{
        didSet{
            updateData()
        }
    }
    
    var setLoding: Bool = false{
        didSet{
            setLoadingAnimation()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setLoadingAnimation(){
        imgCover.image = nil
        lblTitle.text = ""
        lblPrice.text = ""
        imgRatings.text = ""
        
        imgCover.animationLoading()
        lblTitle.animationLoading()
        lblPrice.animationLoading()
        imgRatings.animationLoading()
    }
    
    func updateData(){
        imgCover.stopAnimationLoading()
        lblTitle.stopAnimationLoading()
        lblPrice.stopAnimationLoading()
        imgRatings.stopAnimationLoading()
        
        guard let product = self.setData else {return}
        lblTitle.text = product.title.htmlToString
        lblPrice.text = "$ \(product.getPrice())"
        imgRatings.text = "\(product.customerRating)"
        if let urlImage = URL(string: product.imageUrl){
            imgCover.kf.setImage(with: urlImage)
        }
    }
    
    func updateUI(){
        //containerView.roundCorners([.allCorners], radius: 12)
        addShadow(offset: CGSize(width: -1, height: 2), color: .darkText, radius: 2, opacity: 0.7)
    }

    override func layoutSubviews() {
        updateUI()
    }
    
}
