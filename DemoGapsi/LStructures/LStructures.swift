//
//  LStructures.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import Foundation
import Realm
import RealmSwift


struct LGenericResponse     : Codable{
    var responseStatus      : String = ""
    var responseMessage     : String = ""
    var sortStrategy        : String = ""
    var domainCode          : String = ""
    var keyword             : String = ""
    var numberOfProducts    : Int = 0
    var productDetails      : [LProduct] = []
    
    init() {}
    init(from decoder: Decoder) throws {
        let container       = try decoder.container(keyedBy: CodingKeys.self)
        responseStatus    = try container.decodeIfPresent(String.self, forKey: .responseStatus      ) ?? ""
        responseMessage   = try container.decodeIfPresent(String.self, forKey: .responseMessage     ) ?? ""
        sortStrategy      = try container.decodeIfPresent(String.self, forKey: .sortStrategy        ) ?? ""
        domainCode        = try container.decodeIfPresent(String.self, forKey: .domainCode          ) ?? ""
        keyword           = try container.decodeIfPresent(String.self, forKey: .keyword             ) ?? ""
        numberOfProducts  = try container.decodeIfPresent(Int.self,     forKey: .numberOfProducts   ) ?? 0
        productDetails    = try container.decodeIfPresent([LProduct].self, forKey: .productDetails  ) ?? []
    }
}

struct LProduct             : Codable{
    var productId           : String = ""
    var usItemId            : String = ""
    var title               : String = ""
    var description         : String = ""
    var imageUrl            : String = ""
    var department          : String = ""
    var productCategory     : String = ""
    var customerRating      : Float = 0
    var specialOfferText    : String = ""
    var sellerName          : String = ""
    var primaryOffer        : LProductOffer?
    
    init() {}
    
    init(from decoder: Decoder) throws {
        let container       = try decoder.container(keyedBy: CodingKeys.self)
        productId         = try container.decodeIfPresent(String.self, forKey: .productId       ) ?? ""
        usItemId          = try container.decodeIfPresent(String.self, forKey: .usItemId        ) ?? ""
        title             = try container.decodeIfPresent(String.self, forKey: .title           ) ?? ""
        description       = try container.decodeIfPresent(String.self, forKey: .description     ) ?? ""
        imageUrl          = try container.decodeIfPresent(String.self, forKey: .imageUrl        ) ?? ""
        department        = try container.decodeIfPresent(String.self, forKey: .department      ) ?? ""
        productCategory   = try container.decodeIfPresent(String.self, forKey: .productCategory ) ?? ""
        customerRating    = try container.decodeIfPresent(Float.self,  forKey: .customerRating  ) ?? 0.0
        specialOfferText  = try container.decodeIfPresent(String.self, forKey: .specialOfferText) ?? ""
        sellerName        = try container.decodeIfPresent(String.self, forKey: .sellerName      ) ?? ""
        primaryOffer      = try container.decodeIfPresent(LProductOffer.self, forKey: .primaryOffer    ) ?? LProductOffer()

    }
    
    func getPrice()-> String{
        return "\(primaryOffer?.offerPrice ?? 0) \(primaryOffer?.currencyCode ?? "")"
    }
    
    
}
struct LProductOffer    : Codable{
    var offerId         : String = ""
    var offerPrice      : Float = 0
    var listPrice       : Float = 0
    var currencyCode    : String = ""
    var savingsAmount   : Float = 0
    var showWasPrice    : Bool = true
    
    init() {}
    init(from decoder: Decoder) throws {
        let container       = try decoder.container(keyedBy: CodingKeys.self)
        offerId             = try container.decodeIfPresent(String.self, forKey: .offerId       ) ?? ""
        offerPrice          = try container.decodeIfPresent(Float.self, forKey: .offerPrice     ) ?? 0
        listPrice           = try container.decodeIfPresent(Float.self, forKey: .listPrice      ) ?? 0
        currencyCode        = try container.decodeIfPresent(String.self, forKey: .currencyCode  ) ?? ""
        savingsAmount       = try container.decodeIfPresent(Float.self, forKey: .savingsAmount  ) ?? 0
        showWasPrice        = try container.decodeIfPresent(Bool.self, forKey: .showWasPrice    ) ?? true
    }
}


class LSearchElements : Object{
    @objc dynamic var uuid            : String = UUID().uuidString
    @objc dynamic var word            : String = ""
    @objc dynamic var date            : Date = Date()

    
    override static func primaryKey() -> String? {
        return "uuid"
    }
}

