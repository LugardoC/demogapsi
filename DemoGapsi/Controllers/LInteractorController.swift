//
//  LInteractorController.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import Foundation

class LInteractorController {
    static let shared   = LInteractorController()

    typealias succesSearch       = (LGenericResponse?,Error?)->Void

    
    var actualPage      : Int = 0
    var actualSearch    : String = ""
    
    func getElements(_ query: String,_ page: Int,callBack : @escaping succesSearch){
        LGenericAdaptar.codableRequest(.search(query, page), LGenericResponse.self, succes: { [weak self] data,error in
            if let objet = data{
                print(objet.keyword)
                callBack(objet,nil)
                
            }else if let error = error{
                print(error.localizedDescription)
                callBack(nil,error)
            }else{
                callBack(nil,nil)
            }
        })
    }
    
    func getNextPageNum()-> Int{
        actualPage = actualPage + 1
        return actualPage
    }
    
}
