//
//  LDetailController.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import UIKit

class LDetailController: UIViewController {

    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblRating: UILabel!
    @IBOutlet weak var lblCategories: UILabel!
    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var imgFavorite: UIImageView!
    
    
    var setData : LProduct?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }

    func updateUI(){
        self.navigationController?.setHideBar()
        guard let product  = self.setData else {return}
        if let urlCover = URL(string: product.imageUrl){
            imgCover.kf.setImage(with: urlCover)
        }
        lblTitle.text = product.title.htmlToString
        lblPrice.text = "$ \(product.getPrice())"
        lblRating.text = "\(product.customerRating)"
        lblDescription.text = product.description.htmlToString
        lblCategories.text = product.productCategory
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
