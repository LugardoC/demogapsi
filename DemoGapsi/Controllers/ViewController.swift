//
//  ViewController.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var search = UISearchController(searchResultsController: nil)
    private var refreshControl = UIRefreshControl()

    var interator = LInteractorController.shared
    var searchingText = ""
    var data = LGenericResponse()
    var isLoading : Bool = false
    
    var selectedProduct = LProduct()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        registerCollection()
        configureSearchBar()
        
        LEmptyMessage.shared.addMessageTo(self.view, "Para comenzar, introduce un texto.")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureUI()
    }
    
    func configureUI(){
        if data.productDetails.count == 0{
            LEmptyMessage.shared.addMessageTo(self.view, "Para comenzar, introduce un texto.")
        }

        self.navigationController?.setStyle()
        title = "Home"
        collectionView.contentInset = UIEdgeInsets(top: 30, left: 20, bottom: 10, right: 20)
        refreshControl.addTarget(self, action: #selector(self.getInfo), for: .valueChanged)
        refreshControl.tintColor = .black
        collectionView.addSubview(refreshControl)
        
        let btnHistory = UIBarButtonItem(image: UIImage(named: "icHistory"), style: .plain, target: self, action: #selector(self.openHistory(_:)))
        self.navigationItem.leftBarButtonItem = btnHistory
    }
    
    func registerCollection(){
        collectionView.register(UINib(nibName: "LPreviewCollectionCell", bundle: nil), forCellWithReuseIdentifier: "element")
    }
    
    func configureSearchBar(){
        search.searchBar.delegate                               = self
        search.searchBar.tintColor                              = .white
        search.navigationController?.navigationBar.tintColor    = .white
        search.delegate                                         = self
        self.navigationItem.searchController                    = search
        search.hidesNavigationBarDuringPresentation             = true
        search.isActive                                         = true
        self.navigationItem.hidesSearchBarWhenScrolling         = false
    }

    @objc func getInfo(){
        isLoading = true
        LEmptyMessage.shared.hideMessage()
        refreshControl.beginRefreshing()
        self.collectionView.reloadData()
        
        LDataBaseManager.shared.saveSearchText(self.searchingText)
        
        interator.getElements(self.searchingText, interator.getNextPageNum(), callBack: { [weak self] response, error in
            guard let self = self else {return}
            self.data = LGenericResponse()
            if let error = error{
                print(error.localizedDescription)
            }else if let data = response{
                self.data = data
            }else{
                print("error desconocido")
            }
            self.isLoading = false
            if self.data.productDetails.count == 0{
                LEmptyMessage.shared.addMessageTo(self.view, "Ups!, parece que algo salio mal,intenta de nuevo")
            }
            
            self.refreshControl.endRefreshing()
            self.collectionView.reloadData()
        })
    }
    
    @objc func openHistory(_ sender : UIBarItem){
        self.performSegue(withIdentifier: "sw_History", sender: self)
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let dvc = segue.destination as? LDetailController{
            dvc.setData = self.selectedProduct
        }
        if let history = segue.destination as? LHistoryController{
            history.updateSeearch = { [weak self] key in
                guard let self = self else {return}
                self.searchingText = key
                self.getInfo()
            }
        }
    }
    
}


extension ViewController : UISearchBarDelegate,UISearchControllerDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchingText = searchText
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchingText = ""
        self.search.searchBar.text = ""
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.getInfo()
        self.search.dismiss(animated: true, completion: nil)
    }
    
    func willPresentSearchController(_ searchController: UISearchController) {
        self.search.navigationController?.navigationBar.tintColor           = .white
        searchController.navigationController?.navigationBar.tintColor      = .white
        searchController.navigationItem.backBarButtonItem?.tintColor        = .white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
    }
}



extension ViewController : UICollectionViewDelegate,UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return isLoading ? 6 : data.numberOfProducts
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "element", for: indexPath) as? LPreviewCollectionCell{
            cell.setLoding = self.isLoading
            if !self.isLoading{
                cell.setData = data.productDetails[indexPath.row]
            }
            return cell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if data.productDetails.count > indexPath.row{
            selectedProduct = data.productDetails[indexPath.row]
            self.performSegue(withIdentifier: "sw_Detail", sender: self)
        }
    }
}

extension ViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return calculateCellSize()
    }
    
    func calculateCellSize()-> CGSize{
        let spacing = 35
        let cellForRow = 2
        
        let estimatedWidth = (Int(collectionView.frame.size.width) / cellForRow) - spacing
        
        return CGSize(width: estimatedWidth, height: 240)
    }
}
