//
//  LGenericcDB.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import Foundation
import Realm
import RealmSwift




class LDataBaseManager{
    static let shared   = LDataBaseManager()

    func saveSearchText(_ text: String){
        let srElement = LSearchElements()
        srElement.uuid = UUID().uuidString
        srElement.word = text
        srElement.date = Date()
        
        let actualElements = getElementSaves()
        if !actualElements.contains(text){
            saveToRealm(srElement, LSearchElements.self)
        }
    }
    
    func getHistory()->[String]{
        getElementSaves()
    }
    
    private func saveToRealm<T: Object>(_ element : T,_ type : T.Type){
        do{
            let realm               = try Realm()
            try realm.write {
                realm.create(type, value: element, update: .modified)
            }
        }catch{
            print(error.localizedDescription)
        }
    }
    
    private func getElementSaves()-> [String]{
        var results : [String] = []
        do{
            let realm = try Realm()
            let userInf = realm.objects(LSearchElements.self)
            results = userInf.map({$0.word})
        }catch{
            print(error.localizedDescription)
        }
        return results
    }
    
    func getStructureSaved()->[LSearchElements]{
        var results : [LSearchElements] = []
        do{
            let realm = try Realm()
            let userInf = realm.objects(LSearchElements.self)
            for u in userInf{
                results.append(u)
            }
        }catch{
            print(error.localizedDescription)
        }
        return results
    }
    
}





