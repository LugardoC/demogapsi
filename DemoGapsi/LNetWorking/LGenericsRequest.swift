//
//  LGenericsRequest.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import Foundation
import Moya

enum LTargetRequest{
    case search(_ query: String,_ page: Int)
    
}

extension LTargetRequest : TargetType{
    var baseURL: URL {
        switch self {
        case .search:
            return URL(string: Services.URL_SERVICES)!
        }
    }
    
    var path: String {
        switch self {
        default:
            return "demo-gapsi/search"
        }
    }
    
    var method: Moya.Method {
        switch self {
        default:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .search:
            return .requestParameters(parameters: headers ?? [:], encoding: URLEncoding.default)
        /*default:
            return .requestCompositeParameters(bodyParameters: parameters ?? [:], bodyEncoding: JSONEncoding.default, urlParameters: headers!)*/
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .search(let query, let page):
            let headers = [
                "query": query,
                "page": "\(page)",
                "X-IBM-Client-Id": Services.API_KEY
            ] 
            return headers
        default:
            let headers = [
                "X-IBM-Client-Id": Services.API_KEY
            ]
            return headers
        }
    }
    
    var parameters: [String: Any]? {
        return [:]
    }
}

struct LGenericAdaptar {
    static let provider = MoyaProvider<LTargetRequest>()
    
    typealias succesCodable<T: Codable> = (Response,T?)
    typealias normalError = (Swift.Error) -> Void
    typealias moyaError = (MoyaError) -> Void
    typealias requestSucces<T: Codable> = (T?,Swift.Error?) -> Void
    
    static func request(target: LTargetRequest, success successCallback: @escaping (Response) -> Void, error errorCallback: @escaping (Swift.Error) -> Void, failure failureCallback: @escaping (MoyaError) -> Void) {
        provider.request(target) { (result) in
            switch result {
            case .success(let response):
                if response.statusCode >= 200 && response.statusCode <= 300 {
                    successCallback(response)
                } else {
                    let error = NSError(domain: Services.URL_SERVICES, code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
                    errorCallback(error)
                }
                
            case .failure(let error):
                failureCallback(error)
            }
        }
    }
    
    static func request<T: Codable>(target: LTargetRequest,_ type : T.Type, success successCallback: @escaping (succesCodable<T>) -> Void, error errorCallback: @escaping normalError, failure failureCallback: @escaping moyaError) {
        
        provider.request(target) { (result) in
            switch result {
            case .success(let response):
                if response.statusCode >= 200 && response.statusCode <= 300 {
                    successCallback((response,response.data.getStruct(T.self)))
                } else {
                    let error = NSError(domain: Services.URL_SERVICES, code:0, userInfo:[NSLocalizedDescriptionKey: "Parsing Error"])
                    errorCallback(error)
                }
            case .failure(let error):
                failureCallback(error)
            }
        }
    }
    
    static func codableRequest<T: Codable>(_ target: LTargetRequest,_ type: T.Type,succes successCallback : @escaping requestSucces<T>){
        request(target: target, type, success: { response, object in
            if let data = object{
                successCallback(data,nil)
            }else{
                let gError = NSError(domain: Services.URL_SERVICES, code: response.statusCode, userInfo: ["Error:" : response.debugDescription])
                successCallback(nil,gError)
            }
        }, error: { error in
            successCallback(nil,error)
        }, failure: { error in
            let gError = NSError(domain: Services.URL_SERVICES, code: error.errorCode, userInfo: error.errorUserInfo)
            successCallback(nil,gError)
        })
    }
    
}
