//
//  LViewExtensions.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import UIKit

extension UIView{
    
    func addShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        layer.masksToBounds = false
        layer.shadowOffset = offset
        layer.shadowColor = color.cgColor
        layer.shadowRadius = radius
        layer.shadowOpacity = opacity
        
        let backgroundCGColor = backgroundColor?.cgColor
        backgroundColor = nil
        layer.backgroundColor =  backgroundCGColor
    }
    
    func roundCorners(_ corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func addCircleShadow(offset: CGSize, color: UIColor, radius: CGFloat, opacity: Float) {
        backgroundColor = .clear
        layer.shadowColor = color.cgColor
        layer.shadowOffset = offset
        layer.shadowOpacity = opacity
        layer.shadowRadius = radius
        layer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: bounds.size.width/2).cgPath
        layer.shouldRasterize = true
    }
    
    func animationLoading(){
        stopAnimationLoading()
        
        let gradient        = CAGradientLayer()
        gradient.name       = "gradient"
        gradient.frame      = self.bounds
        gradient.startPoint = CGPoint(x:0.0, y:0.0)
        gradient.endPoint   = CGPoint(x:1.5, y:0.0)
        gradient.colors     = [UIColor(netHex: 0xf6f6f6).withAlphaComponent(0.08).cgColor, UIColor(netHex: 0xbfbfbf).cgColor]
        gradient.locations  =  [0.0, 1.0]
        
        let animation           = CABasicAnimation(keyPath: "colors")
        animation.fromValue     = [UIColor(netHex: 0xf6f6f6).withAlphaComponent(0.08).cgColor, UIColor(netHex: 0xbfbfbf).cgColor]
        animation.toValue       = [UIColor(netHex: 0xbfbfbf).cgColor, UIColor(netHex: 0xf6f6f6).withAlphaComponent(0.08).cgColor]
        animation.duration      = 0.7
        animation.autoreverses  = true
        animation.repeatCount   = Float.infinity
        gradient.add(animation, forKey: nil)
        self.layer.addSublayer(gradient)
    }
    
    func stopAnimationLoading(){
        if let sbLayers = self.layer.sublayers{
            for layer in sbLayers{
                if layer.name == "gradient"{
                    layer.removeFromSuperlayer()
                }
            }
        }
    }
    
    func progressBorderAnimationStop(){
        if let sbLayers = self.layer.sublayers{
            for layer in sbLayers{
                if layer.name == "progressBorderLayer"{
                    layer.removeFromSuperlayer()
                }
            }
        }
                
    }
    
}

extension String{
    var htmlToAttributedString: NSAttributedString? {
        do {
            let font = UIFont.systemFont(ofSize: 16)
            let modifiedFont = NSString(format:"<span style=\"font-family: \(font.fontName); font-size: \(font.pointSize)\">%@</span>" as NSString, self)
            
            let attrStr = try NSAttributedString(
                data: modifiedFont.data(using: String.Encoding.unicode.rawValue, allowLossyConversion: true)!,
                options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue],
                documentAttributes: nil)
            
            return attrStr
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

//Data's Extension
extension Data{
    func getStruct<T: Codable>(_ type : T.Type)-> T?{
        do{
            let struture = try T.decode(data: self)//try JSONDecoder().decode(self, from: self)
            return struture
        }catch{
            print(error.localizedDescription)
        }
        return nil
    }
}

extension Decodable{
    static func decode(data: Data) throws -> Self{
        let decoder = JSONDecoder()
        return try decoder.decode(Self.self, from: data)
    }
}
