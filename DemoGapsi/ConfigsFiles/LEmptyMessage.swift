//
//  LEmptyMessage.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import UIKit

class LEmptyMessage: UIView {

    static let shared = LEmptyMessage()
    
    private var imgHolder = UIImageView(image: UIImage(named: "no-results"))
    private var lblTitle = UILabel()
    private var parentView = UIView()
    private var isSearching = false
    private var paddingTop  : Int = 0
    private var paddingLeft : Int = 0
    
    
    var errorHandler : ((_ error : Bool)->Void)?


    func adjustMessage(){
        imgHolder.translatesAutoresizingMaskIntoConstraints = false
        let  horizontalConstraint = imgHolder.centerXAnchor.constraint(equalTo: centerXAnchor)
        if paddingLeft != 0 {
            horizontalConstraint.constant = CGFloat(paddingLeft) + horizontalConstraint.constant
        }
        let verticalConstraint = imgHolder.centerYAnchor.constraint(equalTo: parentView.centerYAnchor, constant: (CGFloat(-50 + paddingTop)))
        let widthConstraint = isSearching ? imgHolder.widthAnchor.constraint(equalToConstant: 285) : imgHolder.widthAnchor.constraint(equalToConstant: 233)
        let heightConstraint = isSearching ? imgHolder.heightAnchor.constraint(equalToConstant: 274) : imgHolder.heightAnchor.constraint(equalToConstant: 281)
        NSLayoutConstraint.activate([widthConstraint,heightConstraint,horizontalConstraint, verticalConstraint])
        imgHolder.layoutIfNeeded()
        
        
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        let lblTopConstraint = lblTitle.topAnchor.constraint(equalTo: imgHolder.bottomAnchor, constant: 20)
        let lblHorizontalConstraint = lblTitle.centerXAnchor.constraint(equalTo: centerXAnchor, constant: 0)
        if paddingLeft != 0 {
            lblHorizontalConstraint.constant = CGFloat(paddingLeft) + lblHorizontalConstraint.constant
        }
        NSLayoutConstraint.activate([lblTopConstraint,lblHorizontalConstraint])
        lblTitle.layoutIfNeeded()
        
        self.isUserInteractionEnabled = false
        layoutIfNeeded()
    }
    
    func addMessageTo(_ view : UIView,_ text : String,_ image : String? = "no-results",_ paddingTop : Int = 0,_ paddingLeft : Int = 0,_ contentMode : UIView.ContentMode? = .scaleAspectFill){
        isSearching = false
        imgHolder.image     = UIImage(named: image ?? "no-results")
        self.paddingTop     = paddingTop
        self.paddingLeft    = paddingLeft
        addMessage(view, text)
        
        imgHolder.isUserInteractionEnabled = false
        if let errorH = errorHandler{
            errorH(true)
        }
        imgHolder.contentMode = contentMode ?? .scaleAspectFill
        lblTitle.textAlignment = .center
        imgHolder.isUserInteractionEnabled  = false
        isUserInteractionEnabled            = false
    }
    func addSearchMessageTo(_ view : UIView, text : String){
        isSearching = true
        imgHolder.image = UIImage(named: "no-results")
        addMessage(view, text)
        isUserInteractionEnabled            = false
    }
    
    private func addMessage(_ view : UIView,_ text : String){
        hideMessage()
        parentView = view
        parentView.addSubview(self)
        frame = parentView.bounds
        addSubview(imgHolder)
        addSubview(lblTitle)
        lblTitle.text = text
        lblTitle.numberOfLines = 0
        lblTitle.textColor = UIColor(netHex: 0x9b9b9b)
        setNeedsDisplay()
        isUserInteractionEnabled            = false

    }
    
    func hideMessage(){
        removeFromSuperview()
        imgHolder.removeFromSuperview()
        lblTitle.removeFromSuperview()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        adjustMessage()
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
