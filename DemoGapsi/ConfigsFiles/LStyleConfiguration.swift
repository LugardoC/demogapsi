//
//  LStyleConfiguration.swift
//  DemoGapsi
//
//  Created by Juan Carlos Lugardo on 21/06/21.
//

import UIKit


extension UINavigationController{
    
    func setStyle(){
        navigationBar.prefersLargeTitles = true
        navigationBar.tintColor = .white
        navigationBar.barTintColor = UIColor.LETheme.primaryColor
        navigationBar.backgroundColor = UIColor.LETheme.primaryColor
        view.backgroundColor = UIColor.LETheme.primaryColor
        navigationBar.isTranslucent = false
        navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]

    }
    
    func setHideBar(){
        navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationBar.backgroundColor = .clear
        navigationBar.shadowImage = UIImage()
        navigationBar.isTranslucent = true
        navigationBar.tintColor = UIColor.LETheme.primaryColor
        navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.LETheme.primaryColor]
    }
}
